TARGETS = bean
TARGETS_BYTE=$(TARGETS:%=%.byte)

MODULES = bean_ast bean_lexer bean_parser bean_pprint
MLFILES = $(addsuffix .ml, $(MODULES))
CMOFILES = $(addsuffix .cmo, $(MODULES))
CMXFILES = $(addsuffix .cmx, $(MODULES))

ALLMODULES = $(MODULES) bean

OCAMLLEX = ocamllex
OCAMLYACC = ocamlyacc
OCAMLDEP = ocamldep

OCAMLFLAGS =
OCAMLOPTFLAGS =

all : opt
byte: $(TARGETS_BYTE)
opt: $(TARGETS)

parser: bean_lexer.mll bean_parser.mly
	$(OCAMLLEX) bean_lexer.mll
	$(OCAMLYACC) bean_parser.mly

%.cmi : %.mli
	ocamlc $(OCAMLFLAGS) -g -c $<

%.cmx: %.ml
	ocamlopt $(OCAMLOPTFLAGS) -g -c $<

%.cmo: %.ml
	ocamlc $(OCAMLFLAGS) -g -c $<

%.ml: %.mll
	$(OCAMLLEX) $^

%.ml %.mli: %.mly
	$(OCAMLYACC) $^

bean.byte : $(CMOFILES) bean.cmo
	ocamlc -g -o $@ $^

bean : $(CMXFILES) bean.cmx
	ocamlopt -g -o $@ $^

difftest : difftest.cmi difftest.cmx
	ocamlopt $(OCAMLOPTFLAGS) -g -o $@ unix.cmxa difftest.cmx

test : difftest
	@(if command -v diff >/dev/null 2>&1; then \
		printf "\x1b[1mRunning Diff Test \x1b[0m\n" ; \
		./difftest -t ./test-cases/ -e ./bean ; \
	else echo "You need to install the command `diff` for testing."; \
	fi)

test-update : difftest
	@(if command -v dwdiff >/dev/null 2>&1; then \
		printf "\x1b[1mRunning Diff Test \x1b[0m\n" ; \
		./difftest -t ./test-cases/ -e ./bean -u true ; \
	else echo "You need to install the command `dwdiff` for testing."; \
	fi)

submit : bean test
	submit COMP90045 1b README.md Makefile Makefile.depend *.mli *.ml\
		bean_parser.mly bean_lexer.mll Members.txt

verify :
	verify COMP90045 1b

clean :
	rm -f *.cmo *.cmi *.cmx *.o
	rm -f bean_lexer.ml bean_parser.ml bean_parser.mli
	rm -f difftest

clobber : clean
	rm -f $(TARGETS) $(TARGETS_BYTE)

.PHONY : clean clobber depend

# include depend
depend: bean_lexer.ml bean_parser.ml
	$(OCAMLDEP) bean.ml *.mli *.ml >Makefile.depend

-include Makefile.depend
