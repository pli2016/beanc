Bean Compiler - PLI Project
=======================================

The `bean` compiler at this stage can parse a complete bean program and pretty
print its AST.

Along with the compiler, we have created our own test suite which runs a simple
diff test with the obtained output and expected output, as outlined in the
'Testing' section below.


# Running

Compiling the compiler:
    
    make

# Testing

Running tests: Assuming that there are test-cases (.bean) files in a directory
`test-cases`, and for every test-cases `a.bean` there is a 
`test-cases/expected/a.exp` file, which contains the expected output of the
compilation. 

    make test
    
Which just runs `difftest` with some default command line params as:

    difftest -t ./test-cases/ -e ./bean 

If there are failed test cases then a `ERRS` file will be created ellaborating
on the failed cases (contains diff results).

If you have added new test-cases to your `test-cases` folder or you think some
output is correct and would like to quickly updated the expected cases, you can
run `difftest` with the `-u true` param or just run:

    make test-update 
    
This will run a prompt on all error cases, after running the tests again, to
update the respective expected outputs with the new outputs.



# Files

| File            | Description                               |
|-----------------|-------------------------------------------|
| Makefile        | A makefile for the COMP90045 project 2016 |
| Makefile.depend | A listing of the file dependencies        |
| bean.ml         | The main module                           |
| bean_ast.ml     | The data structures that make up the AST  |
| bean_ast.mli    | The interface file for bean_ast.ml        |
| bean_lexer.mll  | The bean lexer specification              |
| bean_parser.mly | An ocamlyacc specification for bean       |
| bean_pprint.ml  | Pretty Printer                            |
| bean_pprint.mli | The interface file for bean_pprint.ml     |
| difftest.ml{i}  | The testing suite.

# Team Members 

Members of our Team `Cocoa Bean`:
- @sangzhuoyang 
- @shivisuper
- @ashutoshrishi
