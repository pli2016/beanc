open Lexing

(* Argument parsing code *)
let infile_name = ref None

type compiler_mode = PrettyPrint | Compile
let mode = ref Compile

(* --------------------------------------------- *)
(*  Specification for command-line options       *)
(* --------------------------------------------- *)
let (speclist:(Arg.key * Arg.spec * Arg.doc) list) =
  ["-p",
     Arg.Unit(fun () -> mode := PrettyPrint),
     " Run the compiler in pretty-printer mode"
  ]


(** Reporting a syntax error by presenting the [Lexing.lexbuf] fields in a more
    readable format. *)
let report_error (lexbuf: Lexing.lexbuf) =
  let pos = lexeme_start_p lexbuf in
  let err_lexeme = lexeme lexbuf in
  let pos_str = Printf.sprintf "[ line: %d | col: %d ] at char '%s'"
      pos.pos_lnum (pos.pos_cnum - pos.pos_bol) err_lexeme in
  print_endline ("Syntax Error: " ^ pos_str) ;
  (* Most common error *)
  if err_lexeme = "end" then print_endline "Maybe you missed a ';'?"


let main () =
  (* Parse the command-line arguments *)
  Arg.parse speclist
      (begin fun fname -> infile_name := Some fname end)
      "bean [-p] [bean source]" ;

  (* Open the input file *)
  let infile = match !infile_name with
  | None -> stdin
  | Some fname -> open_in fname in

  (* Initialize lexing buffer *)
  let lexbuf = Lexing.from_channel infile in
  (* Call the parser, the lexer fails with SynaxError and Parser with
     Parsing.Parse_error. *)
  try (let prog = Bean_parser.program Bean_lexer.token lexbuf in
       match !mode with
       | PrettyPrint ->
         Bean_pprint.print_program Format.std_formatter prog
       | Compile -> print_endline "Sorry, cannot generate code yet.")
  with
  | Parsing.Parse_error -> report_error lexbuf
  | Bean_ast.Syntax_error s -> (print_endline ("Syntax Error: " ^ s);
                                 report_error lexbuf)

let _ = main ()
