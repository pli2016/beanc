(* Specification of an AST for bean *)

type ident = string


(* Primitive and user defined types. *)
type beantype =
  | Bool
  | Int
  | Struct of (ident * beantype) list
  | Named of ident

(* Top level typedefs. *)
type typedef = (beantype * ident)

type lvalue =
  | LId of ident
  | LField of (lvalue * ident)

type binop =
 | Op_add | Op_sub | Op_mul | Op_div
 | Op_eq  | Op_ne  | Op_lt  | Op_le | Op_gt | Op_ge
 | Op_or  | Op_and

type unop =
  | Op_minus | Op_not


(* All different types of expressions.

   We are treating strings as an expression for parsing, but will have a
   semantic constraint on string expressions appearing only in write
   statements. *)
type expr =
  | Ebool  of bool
  | Eint   of int
  | Estr   of string
  | Elval  of lvalue
  | Ebinop of (expr * binop * expr)
  | Eunop  of (unop * expr)
  | Eparen of expr

type rvalue =
  | Rexpr   of expr
  | Rstruct of (ident * rvalue) list


(* We are considering variable declarations, atomic statements, and compound
   statements as a valid [stmt] in a procedure body. *)
type stmt =
  (* Local variable declaration statement *)
  | Local  of (beantype * ident)
  (* Atomic statements *)
  | Assign of (lvalue * rvalue)
  | Read   of lvalue
  | Write  of expr
  | Call   of (ident * expr list)
  (* Compound statements *)
  | If     of (expr * stmt list * stmt list)
  | While  of (expr * stmt list)

(* A procedure parameter. *)
type param =
  | Val of (beantype * ident)
  | Ref of (beantype * ident)


(* We are considering a procedure body to be a [stmt list]. The parser will
   be ensuring that all [Local] variable declaration statements appear
   before the other atomic/compund statements, or quit in a [Syntax_error]. *)
type proc = {
  proc_name : ident ;
  params : param list ;
  body : stmt list
}

type program = {
  decls : typedef list ;
  procs : proc list
}

type t = program


(** Assign precedence values to binary operators. *)
let binop_prec = function
  | Op_mul | Op_div -> 1
  | Op_add | Op_sub -> 2
  | Op_lt  | Op_le | Op_gt | Op_ge -> 3
  | Op_eq  | Op_ne -> 4
  | Op_and -> 5
  | Op_or -> 6

(** Exception representing every error raised in the compiler front-end. *)
exception Syntax_error of string


(** Oz instruction operands *)
type operand =
  | Register of int
  | StackSlot of int
  | IntConstant of int
  | StringConstant of string
  | Label of string
  | Builtin of string


(** Instructions on oz *)
type instruction =
  | Push_stack_frame of operand
  | Pop_stack_frame  of operand
  | Load             of operand * operand
  | Store            of operand * operand
  | Load_address     of operand * operand
  | Load_indirect    of operand * operand
  | Store_indirect   of operand * operand
  | Int_const        of operand * operand
  | String_const     of operand * operand
  | Add_int          of operand * operand * operand
  | Add_offset       of operand * operand * operand
  | Sub_int          of operand * operand * operand
  | Sub_offset       of operand * operand * operand
  | Mul_int          of operand * operand * operand
  | Div_int          of operand * operand * operand
  | Cmp_eq_int       of operand * operand * operand
  | Cmp_ne_int       of operand * operand * operand
  | Cmp_gt_int       of operand * operand * operand
  | Cmp_ge_int       of operand * operand * operand
  | Cmp_lt_int       of operand * operand * operand
  | Cmp_le_int       of operand * operand * operand
  | And              of operand * operand * operand
  | Or               of operand * operand * operand
  | Not              of operand * operand
  | Move             of operand * operand
  | Branch_on_true   of operand * operand
  | Branch_on_false  of operand * operand
  | Branch_uncond    of operand * operand
  | Call             of operand
  | Call_builtin     of operand
  | Return
  | Halt
  | Debug_reg        of operand
  | Debug_slot       of operand
  | Debug_stack
