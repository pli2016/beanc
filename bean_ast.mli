(** Specification of an AST for bean, including helper functions for AST. *)


type ident = string

(** Valid bean type specifications. *)
type beantype =
  | Bool
  | Int
  | Struct of (ident * beantype) list
  | Named of ident

(** Top level typedef declarations. *)
type typedef = (beantype * ident)


(** Left side values of an assignment. *)
type lvalue =
  | LId of ident
  | LField of (lvalue * ident)

(** Binary operator constructors. *)
type binop =
  | Op_add | Op_sub | Op_mul | Op_div
  | Op_eq  | Op_ne  | Op_lt  | Op_le | Op_gt | Op_ge
  | Op_or  | Op_and

(** Unary operators. *)
type unop =
  | Op_minus | Op_not


(** Bean expressions.

    We are treating strings as an expression for parsing, but will have a
    semantic constraint on string expressions appearing only in write
    statements. *)
type expr =
  | Ebool of bool
  | Eint of int
  | Estr of string
  | Elval of lvalue
  | Ebinop of (expr * binop * expr)
  | Eunop of (unop * expr)
  | Eparen of expr              (* Paren wrapped exprs are needed
                                   in the ast for preserving semantic meaning. *)

(** Right side values of an assignment. *)
type rvalue =
  | Rexpr of expr
  | Rstruct of (ident * rvalue) list


(** Statements in a procedure body. *)
type stmt =
  (* Local variable declaration statement *)
  | Local  of (beantype * ident)
  (* Atomic statements *)
  | Assign of (lvalue * rvalue)
  | Read   of lvalue
  | Write  of expr
  | Call   of (ident * expr list)
  (* Compound statements *)
  | If     of (expr * stmt list * stmt list)
  | While  of (expr * stmt list)


(** Proc parameters. *)
type param =
  | Val of (beantype * ident)
  | Ref of (beantype * ident)


(** A complete procedure.

    We are considering a procedure body to be a [stmt list]. The parser will be
    ensuring that all [Local] variable declaration statements appear before the
    other atomic/compund statements, or quit in a [Syntax_error]. *)
type proc = {
  proc_name : ident ;
  params : param list ;
  body : stmt list
}

(** A complete program. *)
type program = {
  decls : typedef list ;
  procs : proc list
}

(** Export the [program] as the main type for the module. *)
type t = program


(* AST related functions. *)

(** Retrieve a precedence value for a [binop].
    Precedence ranges from 1 to 4 with 1 being the highest. *)
val binop_prec : binop -> int


(** Exception representing every error raised in the compiler front-end. *)
exception Syntax_error of string
