{
open Bean_parser

}


let digit = ['0' - '9']
let letter = ['a' - 'z' 'A' - 'Z']
let alpha = letter | '_'
let ident_char = alpha | '\''
let int_const = '-'? digit+
let ident = alpha ident_char*
let eol = '\r' | '\n' | "\r\n"
let comment = '#' [^ '\n' '\r']* eol

rule token = parse

  (* constant values *)
  int_const as lxm                  { INT_CONST(int_of_string lxm) }
  | "true"                          { BOOL_CONST true }
  | "false"                         { BOOL_CONST false }
  | '\"' [^ '\n' '\t' '\"']* '\"' as lxm { STRING_CONST(lxm) }
  
  (* keywords *)
  | "typedef"         { TYPEDEF }
  | "bool"            { BOOL }
  | "int"             { INT }
  | "proc"            { PROC }
  | "ref"             { REF }
  | "val"             { VAL }
  | "while"           { WHILE }
  | "do"              { DO }
  | "od"              { OD }
  | "if"              { IF }
  | "fi"              { FI }
  | "else"            { ELSE }
  | "then"            { THEN }
  | "end"             { END }
  | "read"            { READ }
  | "write"           { WRITE }
  | ":="              { ASSIGN }
  | '('               { LPAREN }
  | ')'               { RPAREN }
  | ';'               { SEMICOLON }
  | '{'               { LCURLY }
  | '}'               { RCURLY }
  | ':'               { COLON }
  | ','               { COMMA }
  | '.'               { DOT }
  | [' ' '\t']        { token lexbuf }     (* skip blanks *)
  | '\n'              { Lexing.new_line lexbuf ; token lexbuf }

  (* operators *)
  | "and"             { AND }
  | "or"              { OR }
  | "not"             { NOT }
  | '='               { EQ }
  | "!="              { NE }
  | ">="              { GE }
  | "<="              { LE }
  | '<'               { LT }
  | '>'               { GT }
  | '+'               { PLUS }
  | '-'               { MINUS }
  | '*'               { MUL }
  | '/'               { DIV }
  
  (* id & others *)
  | ident as lxm      { IDENT lxm }
  | comment           { token lexbuf }
  | eof               { EOF }
  | _                 { raise (Bean_ast.Syntax_error
                              ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
