/* ocamlyacc parser for bean */
%{
open Bean_ast
%}

/* Constant values, ids */
%token <bool> BOOL_CONST
%token <int> INT_CONST
%token <string> STRING_CONST
%token <string> IDENT

/* Keywords */
%token BOOL INT
%token VAL REF PROC
%token WRITE READ
%token TYPEDEF
%token ASSIGN
%token LPAREN RPAREN
%token LCURLY RCURLY
%token DOT
%token SEMICOLON
%token COMMA
%token COLON
%token EOF
%token OTHER
%token WHILE
%token DO
%token OD
%token IF
%token FI
%token ELSE
%token THEN
%token END

/* Operators */
%token EQ NE LT GT GE LE
%token PLUS MINUS MUL DIV AND OR NOT

/* Associations, precedence */
%left OR
%left AND
%left NOT
%nonassoc EQ NE LT LE GT GE
%left PLUS MINUS
%left MUL DIV
%nonassoc UMINUS

%type <Bean_ast.program> program

%start program
%%

/* Start point */
program:
  | decls procs { { decls = List.rev $1 ; procs = List.rev $2 } }


/* Top level TYPEDEFs */
decls :
  | decls decl { $2 :: $1 }
  | { [] }

decl :
  | TYPEDEF typespec IDENT { ($2, $3) }


/* A type specification. */
typespec :
  | BOOL { Bool }
  | INT { Int }
  | IDENT { Named $1 }
  | LCURLY field_list RCURLY { Struct (List.rev $2) }

/* List of fields in a structure type definition, one or more. */
field_list :
  | field_list COMMA field { $3 :: $1 }
  | field { $1 :: [] }
  | error { raise (Syntax_error "Invalid typedef struct syntax.") }

field :
  | IDENT COLON typespec { ($1, $3) }


/* PROCEDURE grammar rules for 1 or more procs. */
procs :
  | procs proc { $2 :: $1 }
  | proc { $1 :: [] }
  | error { raise (Syntax_error "Invalid procedure definition." ) }

/* A procedure definition. */
proc :
  | PROC IDENT LPAREN param_list RPAREN proc_body END
                { {proc_name = $2 ; params = $4 ; body = $6} }


/* Vars, refs formal parameters for the procedure. 0 or more. */
param_list :
  | { [] }
  | params { $1 }

/* Grammar for one or more parameter in the list */
params :
  | param COMMA params { $1 :: $3 }
  | param { $1 :: [] }

/* A param is either a 'val' param or a 'ref' param. */
param :
  | VAL typespec IDENT { Val ($2, $3) }
  | REF typespec IDENT { Ref ($2, $3) }
  


/* Procedure body, of variable declarations (0+) followed by
   statements (1+). It is a `Syntax_error' if there are variable decls
   in between statements. */
proc_body :
  | var_decls stmts { (List.rev $1) @ (List.rev $2) }

/* Sequence of procedure variable declarations, zero or more. */
var_decls :
  | var_decls var_decl { $2 :: $1 }
  | { [] }

var_decl : typespec IDENT SEMICOLON { Local ($1, $2) }


/* Build a list of statements, one or more. */
stmts:
  | stmts stmt { $2 :: $1 }
  | stmt { $1 :: [] }
  | error {
      raise (Syntax_error
        "Procedure body should be 0+ declarations followed by statements.") }

/* Atomic and compound statements. */
stmt :
  | READ lvalue SEMICOLON               { Read $2 }
  | WRITE expr SEMICOLON                { Write $2 }
  | lvalue ASSIGN rvalue SEMICOLON      { Assign ($1, $3) }
  | IDENT LPAREN exprs RPAREN SEMICOLON { Call ($1, List.rev $3) }
  | IF expr THEN stmts FI               { If ($2, List.rev $4, []) }
  | IF expr THEN stmts ELSE stmts FI    { If ($2, List.rev $4, List.rev $6) }
  | WHILE expr DO stmts OD              { While ($2, List.rev $4) }


/* Right part of an assignment */
rvalue :
  | expr { Rexpr $1 }
  | LCURLY rfields RCURLY { Rstruct (List.rev $2) }

/* rvalue fields */
rfields :
  | rfields COMMA rfield { $3 :: $1 }
  | rfield { $1 :: [] }
  | { [] }

rfield :
  | IDENT EQ rvalue { ($1, $3) }


/* Left part of an assignment */
lvalue:
  | IDENT { LId $1 }
  | lvalue DOT IDENT  { LField ($1, $3) }


/* Expressions */
exprs:
  | exprs COMMA expr { $3 :: $1 }
  | expr { $1 :: [] }
  | { [] }

expr:
  | BOOL_CONST              { Ebool $1 }
  | INT_CONST               { Eint $1 }
  | STRING_CONST            { Estr $1 }
  | lvalue                  { Elval $1 }

  /* Binary operators */
  | expr PLUS expr          { Ebinop ($1, Op_add, $3) }
  | expr MINUS expr         { Ebinop ($1, Op_sub, $3) }
  | expr MUL expr           { Ebinop ($1, Op_mul, $3) }
  | expr DIV expr           { Ebinop ($1, Op_div, $3) }
  | expr EQ expr            { Ebinop ($1, Op_eq, $3) }
  | expr NE expr            { Ebinop ($1, Op_ne, $3) }
  | expr LT expr            { Ebinop ($1, Op_lt, $3) }
  | expr LE expr            { Ebinop ($1, Op_le, $3) }
  | expr GT expr            { Ebinop ($1, Op_gt, $3) }
  | expr GE expr            { Ebinop ($1, Op_ge, $3) }
  | expr AND expr           { Ebinop ($1, Op_and, $3) }
  | expr OR expr            { Ebinop ($1, Op_or, $3) }

  /* Unary */
  | NOT expr                { Eunop (Op_not, $2) }
  | MINUS expr %prec UMINUS { Eunop (Op_minus, $2) }
  | LPAREN expr RPAREN      { Eparen $2 }
