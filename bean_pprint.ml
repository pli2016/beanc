open Bean_ast
open Format


(*****************************************************************************)
(* Helpers                                                                   *)
(*****************************************************************************)

(** Easy access synonym for printing keywords to `formatter. *)
let kwd = pp_print_string


(** Split the [list1] into two lists wrapped in a tuple, with the first
    list containing values from [list1] on which [pred] is true, and the
    second list containing the other values. *)
let split_on pred list1 =
  let rec collect from_list trues falses =
    match from_list with
    | []      -> (trues, falses)
    | x :: xs ->
      if pred x
      then collect xs (List.append trues [x]) falses
      else collect xs trues (List.append falses [x]) in
  collect list1 [] []


(** Helper which performs [f] on every element of the list [iter_list] just
    like iter, but also performs [sep_action] after every but the last
    element (just like a separator). *)
let rec inter_iter f iter_list sep_action =
  match iter_list with
  | [] -> ()
  | hd :: [] -> f hd
  | hd :: tl -> f hd; sep_action (); inter_iter f tl sep_action


(** Helper to print a ", " separator to the [fmt] buffer as a field separator. *)
let comma_separator fmt () = kwd fmt ","; pp_print_space fmt ()


(** Helper to check for null list *)
let null_list = function
  | _ :: _ -> false
  | _ -> true


(*****************************************************************************)
(* Pretty Printing functions                                                 *)
(*****************************************************************************)


(** Print the string representation of a [beantype] to the formatter [fmt]. *)
let rec show_type fmt = function
  | Bool -> kwd fmt "bool"
  | Int -> kwd fmt "int"
  | Named s -> kwd fmt s

  (* { val: ty, ... } *)
  | Struct fields ->
    kwd fmt "{";
    (* Each field is wrapped in a hovbox to enable break hints inside
       the field. *)
    let show_field (id, ty) = (pp_open_hovbox fmt 2;
                               kwd fmt id ;
                               kwd fmt " : ";
                               show_type fmt ty;
                               pp_close_box fmt ()) in
    inter_iter show_field fields (comma_separator fmt) ;
    kwd fmt "}"


(** Print the string representation of a [typedef] to the formatter [fmt]. *)
let show_typedef fmt (spec, name) =
  pp_open_hovbox fmt 4;
  kwd fmt "typedef ";
  show_type fmt spec;
  pp_print_space fmt ();
  kwd fmt name;
  pp_close_box fmt ();
  pp_print_newline fmt ()


(** Return a [string] representation of a [lvalue]. *)
let rec lvalue_repr = function
  | LId id -> id
  | LField (lvalue, ident) -> lvalue_repr lvalue ^ "." ^ ident


(** Return a [string] representation of a binary operator type [binop]. *)
let binop_repr = function
  | Op_add -> " + "
  | Op_sub -> " - "
  | Op_mul -> " * "
  | Op_div -> " / "
  | Op_eq -> " = "
  | Op_ne -> " != "
  | Op_lt -> " < "
  | Op_le -> " <= "
  | Op_gt -> " > "
  | Op_ge -> " >= "
  | Op_or -> " or "
  | Op_and -> " and "


(** Return a [string] representation of an unary operator type [unop]. *)
let unop_repr = function
  | Op_minus -> "-"
  | Op_not -> "not "


(** Return a [string] represenation of an [expr] assuming the expression has an
    enclosing context precedence [prec].

    The enclosing precedence is used to decide whether the expression needs to
    keep parenthesis around itself (if it has one), using a few ground rules:
    - parens are never added, only kept or removed
    - If the enclosing precedence is higher then parens are not needed.
    - If the enclosing precedence is equal to or lesser than the enclosed expr,
    parens are needed.
    - For binary operations, due to left associativeness, the left operand can
    omit it's parens iff the current operation has equal or higher precedence.

    An enclosing precedence of 10, is lower than valid prec values.

    Note: A low precedence value means higher precedence.
*)
let rec expr_repr prec = function
  | Ebool b -> string_of_bool b
  | Eint i -> string_of_int i
  | Estr s -> s
  | Elval lvalue -> lvalue_repr lvalue

  | Ebinop (expr1, op1, expr2) ->
    let new_prec = binop_prec op1 in
    (* Due to left associativeness of binary operators, left side operands
       being a binary operation of equal or higher precedence do not need
       parentheses. [calc_left_prec expr] computes needed enclosing prec for
       [expr]. An enclosing prec of 10 will override parens. *)
    let rec calc_left_prec ex = (match ex with
        | Eunop _ -> 0
        | Ebinop (_, op2, _) ->
          if binop_prec op2 > new_prec then new_prec else 10
        | Eparen ex2 -> calc_left_prec ex2
        | _ -> new_prec) in
    let left_prec = calc_left_prec expr1 in
    expr_repr left_prec expr1 ^ binop_repr op1 ^ expr_repr new_prec expr2

  (* Unary operators get the highest precedence, 0 *)
  | Eunop (unop, expr) -> unop_repr unop ^ expr_repr 0 expr

  (* Depending on the enclosing precedence, choose whether to omit the parens. *)
  | Eparen exp ->
    (match exp with
     | Ebinop (_, op, _) ->
       let new_prec = binop_prec op in
       if prec <= new_prec
       then "(" ^ (expr_repr new_prec exp) ^ ")"
       else expr_repr new_prec exp
     | _ -> expr_repr prec exp)


(** Print a [rvalue] in a hovbox. *)
let rec rvalue_show fmt = function
  (* We assume expression contexts which are not expressions have the lowest
     prec, say 10. This prevents unnecessary parens. *)
  | Rexpr rexpr -> kwd fmt (expr_repr 10 rexpr)

  (* Rvalue of fields is wrapped in another hovbox to enable multiline
     formatting (not a requirement but just in case). *)
  | Rstruct fields ->
    pp_open_hovbox fmt 2;
    kwd fmt "{";

    let show_field (id, rval) =
      kwd fmt id; kwd fmt " =";
      pp_print_space fmt ();
      rvalue_show fmt rval in
    inter_iter show_field fields (comma_separator fmt);

    kwd fmt "}" ;
    pp_close_box fmt ()


(** Printing of atomic statements to formmater, in a regular. hovbox. *)
let show_atomic_stmt fmt stmt =
  pp_open_hovbox fmt 0;
  (match stmt with
   (* type var = rval; *)
   | Assign (lvalue, rvalue) ->
     kwd fmt (lvalue_repr lvalue);
     pp_print_space fmt (); kwd fmt ":="; pp_print_space fmt ();
     rvalue_show fmt rvalue

   (* read var; *)
   | Read lvalue ->
     kwd fmt "read ";
     kwd fmt (lvalue_repr lvalue)

   (* write var; *)
   | Write expr ->
     kwd fmt "write ";
     kwd fmt (expr_repr 10 expr)

   (* type var; *)
   | Local (ty, id) ->
     show_type fmt ty ; kwd fmt " " ; kwd fmt id

   (* fun(...); *)
   | Call (id, exprs) ->
     let call_list = String.concat ", " (List.map (expr_repr 10) exprs) in
     kwd fmt id; kwd fmt "("; kwd fmt call_list; kwd fmt ")"

   (* We won't be calling this func on compound statements, but catch that. *)
   | _ -> kwd fmt "NOP" );
  kwd fmt ";";
  pp_close_box fmt ()


(** Printing of a statement body in a vbox.
    Compound statements will make recursively apply show_body to their
    own statement list (body).
 *)
let rec show_body fmt body =
  (* Indent a list of [stmt] by 4 spaces by placing a break hint in the
     external vbox, and printing the list in a new vbox. *)
  let indent_block bl = (pp_print_break fmt 0 4;
                         pp_open_vbox fmt 0;
                         show_body fmt bl;
                         pp_close_box fmt ();
                         pp_print_break fmt 0 0;) in
  (match body with
   | [] -> ()

   (* PPrinting for Composite If Statement *)
   | If (cond, then_body, else_body) :: rest ->
     pp_open_vbox fmt 0;
     kwd fmt "if " ;
     kwd fmt (expr_repr 10 cond);
     kwd fmt " then" ;

     indent_block then_body;

     (* If blocks with empty else list do not need else statements *)
     (if List.length else_body > 0
      then (kwd fmt "else";
            indent_block else_body)) ;
     kwd fmt "fi";
     pp_close_box fmt ();
     (* End IF *)

     (* Break a newline only when the current stmt is not the last statement.
        to avoid printing extra newlines. *)
     if List.length rest > 0 then pp_print_break fmt 0 0;
     show_body fmt rest

   (* PPrinting for Composite While Statement *)
   | While (cond, body) :: rest ->
     pp_open_vbox fmt 0;
     kwd fmt "while ";
     kwd fmt (expr_repr 10 cond);
     kwd fmt " do";

     indent_block body;

     kwd fmt "od";
     pp_close_box fmt ();
     (* Break a newline if the current stmt is not the last statement. *)
     if List.length rest > 0 then pp_print_break fmt 0 0;
     show_body fmt rest

   (* PPrinting for simpler atomic statements *)
   | stmt :: rest ->
     show_atomic_stmt fmt stmt;
     (* Break a newline if the current stmt is not the last statement. *)
     if List.length rest > 0 then pp_print_break fmt 0 0 ;
     show_body fmt rest)


(** Print a [param] in a hovbox in the [formatter]. *)
let show_param fmt param =
  (* Helper for printing "type id" *)
  let show_val ty id = (show_type fmt ty;
                        pp_print_space fmt () ;
                        kwd fmt id) in
  pp_open_hovbox fmt 0;
  (match param with
  | Val (ty, id) ->
    kwd fmt "val "; show_val ty id
  | Ref (ty, id) ->
    kwd fmt "ref "; show_val ty id
  );
  pp_close_box fmt ()






(** Print procedures in a vbox.
    A 0 column aligned vbox contains the whole proc. After printing the proc
    header (proc name...) horizontally, a 4 column indented vbox is opened
    to contain the procedure body block. For every sub block (if, while)
    a similar +4 column indented vbox is opened. Proceeding every box close,
    is -4 column break (except the first vbox), to properly display a nested
    structure.

    Sample structure:
    |------------------------|
    |proc name (...)         |
    |###|[atomic statement]; |
    |   |if/while...         |
    |   |---------------------
    |   |###|                |
    |   |   |     nested     |
    |   |   |      vbox      |
    |   |   |                |
    |   |###|----------------|
    |   |[atomic statement]; |
    |###|--------------------|
    |end                     |
    |------------------------|
 *)
let show_proc fmt { proc_name = pname ; params = param_list ;
                    body = body_stmts } =
  pp_open_vbox fmt 0;

  (* Proc header *)
  pp_open_hovbox fmt 5;
  kwd fmt "proc "; kwd fmt pname;  kwd fmt "(";
  inter_iter (show_param fmt) param_list (comma_separator fmt);
  kwd fmt ")";
  pp_close_box fmt ();
  (* End header *)

  (* Start body *)
  pp_print_break fmt 0 4;

  pp_open_vbox fmt 0;
  let is_decl st =
    (match st with
     | Local _ -> true
     | _ -> false) in
  (* Separating the declaration statements and the rest of the body *)
  let (decls, rest_stmts) = split_on is_decl body_stmts in
  (* Printing declarations within the procedure first. *)
  if not (null_list decls) then (show_body fmt decls;
                                 pp_print_break fmt 0 0);
  pp_print_break fmt 0 0;

  (* Printing the statements which form the proc body_stmts *)
  show_body fmt rest_stmts;
  pp_close_box fmt ();
  (* End body *)

  (* Decrease indent back to first column. *)
  pp_print_break fmt 0 (-4);
  kwd fmt "end" ;
  pp_print_newline fmt ();
  pp_close_box fmt ()


(** Pretty printing for the [program] ast type.
    Note: Record field punning not available for OCaml 3.11 :( *)
let print_program fmt { decls = ds; procs = ps } =
  List.iter (show_typedef fmt) ds;
  pp_print_newline fmt ();

  inter_iter (show_proc fmt) ps (fun () -> pp_print_newline fmt ())
