(** Pretty Printing AST

    The entire program visual structure consists of [typedef]s followed by
    [proc]s. Typedefs are printing on their own lines. Procs are wrapped in a
    vbox.

    A vbox breaks the line on every break hint which is equivalent to a well
    structured program having every atomic statement on it's own
    line. Recursively defining each sub-block of a proc block in a vbox allows
    easy 4-spaces indentation and un-indentation. Each individual line may be
    wrapped in it's own hovbox.
*)

(** Pretty print the AST structure on the given [Format.formatter]. *)
val print_program : Format.formatter -> Bean_ast.t -> unit
