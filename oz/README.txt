This directory contains the C source files for an Oz emulator.
It assumes suitable versions of flex and bison have been installed.
To generate the emulator, simply type

  make

This will generate an executable called oz which takes the name
of an Oz file as its command line argument.

The generation has been tested in April 2016 on the MSE server

  dimefox.eng.unimelb.edu.au

